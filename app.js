const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const https = require('https');
const fs = require('fs');
const urls = require('./urls.js');
const DataBase = require('./database.js');


const hostname = '127.0.0.1';
const http_port = 8080;
const https_port = 8443;

const credentials = {
    key: fs.readFileSync('credentials/key.pem'),
    cert: fs.readFileSync('credentials/cert.pem')
};

DataBase.setup();

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(http_port);
httpsServer.listen(https_port);

urls_get = urls.filter(e => e.method == 'get')
urls_get.map(e => { app.get(e.url, e.view) });

urls_post = urls.filter(e => e.method == 'post')
urls_post.map(e => { app.post(e.url, e.view) });

urls_put = urls.filter(e => e.method == 'put')
urls_put.map(e => { app.put(e.url, e.view) });

urls_delete = urls.filter(e => e.method == 'delete')
urls_delete.map(e => { app.delete(e.url, e.view) });
