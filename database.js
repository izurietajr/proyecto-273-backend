const sqlite3 = require("sqlite3").verbose();

const db_url = './database/database.sqlite';

class DataBase {

    static setup () {
        const query = `CREATE TABLE IF NOT EXISTS persons ( 
                        pk INTEGER PRIMARY KEY,
                        username TEXT NOT NULL,
                        password TEXT NOT NULL,
                        first_name TEXT NULL,
                        last_name TEXT NULL,
                        email TEXT NOT NULL UNIQUE);`;

        //this.query(query);
        let db = new sqlite3.Database(db_url);
        db.serialize(() => {
          db.each(query, (err, row) => {
            if (err) {
              console.error(err.message);
            }
            console.log(row.id + "\t" + row.name);
          });
        });
        db.close();
    }

    static query (query) {
        let db = new sqlite3.Database(db_url, err => {
            console.log("error")
        });
        db.run(query);
        db.close();
    }
}

module.exports = DataBase;
