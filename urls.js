const Views = require("./views.js")

// urls de la aplicación, con sus vistas respectivas

module.exports = [

    {
        url: '/',
        method: 'get',
        view: Views.infoView
    },
    {
        url: '/create/',
        method: 'post',
        view: Views.createPerson
    },
    {
        url: '/login/',
        method: 'post',
        view: Views.loginView
    },
]
