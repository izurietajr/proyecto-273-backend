const DataBase = require('../database.js');

class Person { // Models

    constructor(username, password, email){
        this._email = email;
        this._username = username;
        this._password = password;
        this._first_name;
        this._last_name;
        this._email;
        this._pk
    }


    get pk () {
        console.log("getting pk")
        return this._pk
    }

    set pk (pk) {
        console.log("setting pk")
        this._pk = pk
    }

    static query(query){
        console.log("checking for query", query, "in db");

        let p = [new this("test", "123", "test@mial.com")]

        let keys = Object.keys(query)
        return p.filter(e => e[keys[0]] == query[keys[0]])
    }

    save () {
        DataBase.query(`INSERT INTO persons(username, email, password)
                VALUES('${this._username}', '${this._email}', '${this._password}')`);
        return this;
    }

}

module.exports = Person;
