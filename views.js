const Person = require("./models/person.js");

// vistas de la aplicación

class Views {

    static infoView (req, res) {
        // GET
        // información del servicio
        res.setHeader('Content-Type', 'application/json');
        var response_msg = {
            message: "Inicia sesión con tu nombre de usuario y contraseña",
        }
        res.send(response_msg);
    }

    static createPerson (req, res) {
        let email = req.body.email;
        let username = req.body.username;
        let password = req.body.password;
        if(Person.query({email: email}).length > 0){
            let response = {message: "esta persona ya está registrada"}
            res.send(response);
        }
        else {
            let person = new Person(username, password, email);
            person.save()
            let response = {
                message: "usuario creado",
                data: person
            }
            res.send(response);
        }
    }
    
    static createClient (req, res) {
        /* creación de un cliente */
    }

    static loginView (req, res) {
        // POST
        console.log(req.body)
        res.setHeader('Content-Type', 'application/json');
        res.send(req.body);
    }

}

module.exports = Views;
