
# Obtención de certificados SSL

Un certificado ssl puede obtenerse por parte de una autoridad certificadora, o en este caso, con finalidad educativa, puede ser generado por nosotros mismos.  Haremos esto con los comandos:

```bash
openssl genrsa -out key.pem
openssl req -new -key key.pem -out csr.pem
openssl x509 -req -days 9999 -in csr.pem -signkey key.pem -out cert.pem
rm csr.pem
```
